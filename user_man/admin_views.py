from django.shortcuts import render
from django.utils import timezone
from rest_framework import permissions, renderers, status, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, action
from rest_framework.reverse import reverse
from .serializers import *
from .ressources import *
from .models import *
from tablib import Dataset
from django.http import HttpResponse
from rest_framework.parsers import FileUploadParser


# Gestion des utulisateurs ou comptes start

class AdminUserViewSet(viewsets.ModelViewSet):
    queryset = Profil.objects.filter(deleted=False)
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, permissions.IsAdminUser]

    # Methodes suplementaire de compte start
    def destroy(self, request, pk=None):
        objet = Profil.objects.get(pk=pk)
        try:
            # objet.deleted = True
            # objet.deleted_at = timezone.now()
            objet.suppress()
            objet.save()
            return Response({'status': 'Suppression faite'})
        except:
            return Response({'status': 'Erreur lors de la suppression'})

    @action(detail=True, permission_classes=[permissions.IsAuthenticatedOrReadOnly, permissions.IsAdminUser])
    def restore(self, request, pk=None):
        objet = Profil.objects.get(pk=pk)
        try:
            # objet.deleted = False
            # objet.deleted_at = None
            objet.restore()
            objet.save()
            return Response({'status': 'Les données ont été restaurées avec succes'})
        except:
            return Response({'status': 'Erreur lors de la suppression'})

    @action(detail=False, permission_classes=[permissions.IsAuthenticatedOrReadOnly, permissions.IsAdminUser])
    def get_deleted_list(self, request):
        objet = Profil.objects.filter(deleted=True).order_by('-deleted_at')
        serializer = self.get_serializer(objet, many=True)
        return Response(serializer.data)

    # Methodes suplementaire de compte end

# Gestion des utulisateurs ou comptes end