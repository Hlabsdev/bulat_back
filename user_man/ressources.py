from import_export import resources, fields, widgets
from .models import *
from django.contrib.auth.models import User
from .models import *


class ProfilRessource(resources.ModelResource):
    class Meta:
        model = Profil
        fields = ('email', 'nom', 'prenom', 'date_naissance', 'contact')

# class UserRessource(resources.ModelResource):
#     email= fields.Field(widget=widgets.ForeignKeyWidget, )
#     nom
#     prenom
#     date_naissance
#     contact
#     role
#     sexe
#     class Meta:
#         model = Profil
#         import_id_fields = ['id']
#         skip_unchanged = True
#         report_skipped = False
#         fields = ('username', 'password', 'email', 'nom', 'prenom', 'date_naissance', 'contact', 'role', 'sexe')
#         use_bulk = True

class ProfilRessource(resources.ModelResource):
    username = fields.Field(column_name='username', attribute='username', widget=widgets.ForeignKeyWidget(User, 'username'))
    password = fields.Field(column_name='password', attribute='password', widget=widgets.ForeignKeyWidget(User, 'password'))
        
    class Meta:
        model = Profil
        # import_id_fields = ['email',]
        skip_unchanged = True
        report_skipped = False
        fields = ('username', 'password', 'email', 'nom', 'prenoms', 'dateNaissance', 'contact', 'role', 'sexe')
        # fields = ('email', 'nom', 'prenoms', 'dateNaissance', 'contact', 'role', 'sexe')
        use_bulk = True

        # def before_save_instance(instance, using_transactions, dry_run){
        # # }
        # """ def before_import(self, dataset, dry_run):
        #     """
        #     Make standard corrections to the dataset before displaying to user
        #     """

        #     i = 0
        #     last = dataset.height - 1

        #     # for all lines, search for id of family given and add a new line at the bottom with it then delete the first one
        #     while i <= last:
        #         # Check if the Genus exist in DB
        #         if (Profil.objects.filter(email=dataset.get_col(2)[0].capitalize())):
        #             id = Profil.objects.filter(email=dataset.get_col(2)[0].capitalize())[0].id
        #         else :
        #             id = ''
        #         # Check if the family exists in DB
        #         try:
        #             User.objects.get(username=dataset.get_col(2)[0].capitalize())
        #         except TFamilies.DoesNotExist:
        #             raise Exception("User not in DB !")                   
        #         except TFamilies.MultipleObjectsReturned:
        #             pass

        #         # use of "filter" instead of "get" to prevent duplicate values, select the first one in all cases
        #         dataset.rpush((id_genus,
        #             dataset.get_col(1)[0],
        #             TFamilies.objects.filter(name_fam=dataset.get_col(2)[0].capitalize())[0].id_fam))
        #         dataset.lpop()
        #         i = i + 1
        #          """
