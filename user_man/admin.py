from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from .models import *
from django.contrib.admin import AdminSite
from django.utils.translation import ugettext_lazy
from .ressources import *
from django.contrib.admin import AdminSite
from django.utils.translation import ugettext_lazy
from import_export.admin import ImportExportModelAdmin
from import_export import resources

class MyAdminSite(AdminSite):
    # Text to put at the end of each page's <title>.
    site_title = ugettext_lazy('Bulats-CIC Admin')

    # Text to put in each page's <h1> (and above login form).
    site_header = ugettext_lazy('Bulats-CIC Administration')

    # Text to put at the top of the admin index page.
    index_title = ugettext_lazy('Bulats-CIC Administration')

admin_site = MyAdminSite()
# admin.site.site_header = "Bulat-CIC Administration"



class ProfilInline(admin.StackedInline):
    model = Profil
    can_delete = False
    verbose_name = 'profil'

# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (ProfilInline,)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Niveau)

#admin.site.register(Profil)

admin.site.register(Salon)
admin.site.register(EtudiantSalon)
admin.site.register(Invitation)
admin.site.register(TypeTest)
admin.site.register(Test)
admin.site.register(EtudiantTest)
admin.site.register(Historique)
admin.site.register(TypeQuestion)
admin.site.register(Tag)
admin.site.register(Texte)
admin.site.register(Question)
admin.site.register(QuestionTag)
admin.site.register(TestTag)
admin.site.register(Proposition)
admin.site.register(Reponse)


class ProfileImportAdmin(ImportExportModelAdmin):
    resource_class=ProfilRessource

admin.site.register(Profil, ProfileImportAdmin)
