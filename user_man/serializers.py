from rest_framework import serializers
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth.models import update_last_login
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from .models import *


class UserTokenSerializer(serializers.Serializer):
    """get token for user"""
    username = serializers.CharField(required=True, max_length=150)
    password = serializers.CharField(required=True, style={'input_type': 'password'})

class NiveauSerializer(serializers.ModelSerializer):
    class Meta:
        model = Niveau
        exclude = ['deleted']
        read_only_fields = ['created_at', 'updated_at', 'deleted_at', 'url'] 

class ProfilSerializer(serializers.ModelSerializer):
    niveauBulats = NiveauSerializer(read_only=True)

    class Meta:
        model = Profil
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    profil = ProfilSerializer(read_only=True)

    class Meta:
        model = Profil
        fields = '__all__'


class UserProfilSerializer(serializers.ModelSerializer):
    username = serializers.CharField()
    password = serializers.CharField(style={'input_type': 'password'})
    class Meta:
        model = Profil
        fields = ['id', 'user', 'username', 'password', 'email', 'nom', 'prenoms', 'dateNaissance', 'contact', 'role', 'sexe', 'avatar','specialite', 'fonction', 'codeEnseignant', 'codeEtudiant', 'niveauBulats']
        extra_kwargs = {
            'user': {'read_only': True},
            'password': {'write_only': True},
        }

    def create(self, validated_data):
        user = User(
            username=validated_data['username'],
        )
        user.set_password(validated_data['password'])
        user.save()

        utilisateur = Profil.objects.create(
            user=user, 
            email=validated_data['email'],
            nom=validated_data['nom'],
            prenoms=validated_data['prenoms'],
            dateNaissance=validated_data['dateNaissance'],
            contact=validated_data['contact'],
            role=validated_data['role'],
            sexe=validated_data['sexe'],
            avatar=validated_data['avatar'],
            specialite=validated_data['specialite'],
            fonction=validated_data['fonction'],
            codeEnseignant=validated_data['codeEnseignant'],
            codeEtudiant=validated_data['codeEtudiant'],
            niveauBulats=validated_data['niveauBulats'],
        )
        return utilisateur

    def update(self, instance, validated_data):
        user = instance.user

        instance.email=validated_data['email']
        instance.nom=validated_data['nom']
        instance.prenoms=validated_data['prenoms']
        instance.dateNaissance=validated_data['dateNaissance']
        instance.contact=validated_data['contact']
        instance.role=validated_data['role']
        instance.sexe=validated_data['sexe']
        instance.avatar=validated_data['avatar']
        instance.specialite=validated_data['specialite']
        instance.fonction=validated_data['fonction'],
        instance.codeEnseignant=validated_data['codeEnseignant']
        instance.codeEtudiant=validated_data['codeEtudiant']
        instance.niveauBulats=validated_data['niveauBulats']
        instance.save()

        # instance.username = validated_data.get('username', instance.username)

        user.username = validated_data['username']
        user.set_password(validated_data['password'])
        user.save()
        return instance


class SalonSerializer(serializers.ModelSerializer):
    enseignant = UserSerializer(read_only=True)

    class Meta:
        model = Salon
        fields = '__all__'


class EtudiantSalonSerializer(serializers.ModelSerializer):
    etudiant = UserSerializer(read_only=True)
    salonBulats = SalonSerializer(read_only=True)

    class Meta:
        model = EtudiantSalon
        fields = '__all__'


class InvitationSerializer(serializers.ModelSerializer):
    etudiant = UserSerializer(read_only=True)
    salonBulats = SalonSerializer(read_only=True)

    class Meta:
        model = Invitation
        fields = '__all__'