from django.db import models
import uuid
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse
from django.db.models.signals import post_save


# Create your models here


class ModelBase (models.Model):
    created_at = models.DateTimeField("Date de création", auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField("Date de mise ajour", auto_now=True, blank=True, null=True)
    deleted = models.BooleanField(default=False, blank=True, null=True)
    deleted_at = models.DateTimeField("Date de suppression", blank=True, null=True)

    def __suppress__(self):
        self.deleted = True
        self.deleted_at = timezone.now

    def __restore__(self):
        self.deleted = False
        self.deleted_at = None


class Niveau(ModelBase):
    libelle = models.CharField(null=False, blank=False, max_length=255)
    description = models.TextField(null=True, blank=True)
    class Meta:
        verbose_name = ("niveau")
        verbose_name_plural = ("niveaux")

    
class Profil(ModelBase):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profil', blank=True, null=True)
    email = models.EmailField("email", max_length=254, blank=True, null=True)
    nom = models.CharField(blank=True, null=False, max_length=50)
    prenoms = models.CharField(blank=True, null=False, max_length=150)
    dateNaissance = models.DateField(
        "date de naissance de l'apprenant", blank=True, null=True)
    contact = models.CharField(max_length=12, blank=True, null=True)
    TYPE_PROFIL = (
        ('Admin', 'Admin'),
        ('Enseignant', 'Enseignant'),
        ('Apprenant', 'Apprenant'),
        ('Remplisseur', 'Remplisseur'),
    )
    role = models.CharField(
        max_length=15, choices=TYPE_PROFIL, blank=True, null=False)

    TYPE_SEXE = (
        ('M', 'Masculin'),
        ('F', 'Féminin')
    )
    sexe = models.CharField(max_length=1, choices=TYPE_SEXE, blank=True, null=True)

    avatar = models.ImageField(
        upload_to="User/Profile", blank=True, null=True, verbose_name="Image de profil")
    actif = models.BooleanField(default=False, blank=True, null=True)
    valide = models.BooleanField(default=False, blank=True, null=True)
    specialite = models.CharField(null=True, blank=True, max_length=255)
    fonction = models.CharField(null=True, blank=True, max_length=150)
    codeEnseignant = models.CharField(null=True, blank=True, max_length=50)
    codeEtudiant = models.CharField(null=True, blank=True, max_length=50)
    niveauBulats = models.ForeignKey(Niveau, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return "user - {0}".format(self.user.id)

    def __unicode__(self):
        return self.user

    def upload_fichier(self, filename):
        path = 'File/Visite/{}'.format(filename)
        return path


class Salon(ModelBase):
    nom = models.CharField(null=False, blank=False, max_length=255)
    statut = models.CharField(null=False, blank=False, max_length=255)
    locked = models.BooleanField(null=True, blank=True, default=False)
    visibilite = models.BooleanField(null=True, blank=True, default=False)
    objectifs = models.TextField(null=True, blank=True)
    enseignant = models.ForeignKey(User, null=False, on_delete=models.CASCADE)

    
class EtudiantSalon(ModelBase):
    salonBulats = models.ForeignKey(Salon, on_delete=models.CASCADE, null=False)
    etudiant = models.ForeignKey(User, on_delete=models.CASCADE, null=False)


class Invitation(ModelBase):
    dateInvitation = models.DateTimeField("Date d'invitation", auto_now_add=True, blank=True, null=False)
    etat = models.CharField(null=True, blank=True, max_length=40)
    salonBulats = models.ForeignKey(Salon, on_delete=models.CASCADE, null=False)
    etudiant = models.ForeignKey(User, on_delete=models.CASCADE, null=False)


class TypeTest(ModelBase):
    libelle = models.CharField(null=False, blank=False, max_length=255)
    ndListening = models.IntegerField(null=True)
    nbReading = models.IntegerField(null=True)


class Test(ModelBase):
    dateDebut = models.DateTimeField("Date de début", null=True)
    dateLimite = models.DateTimeField("Date limite", null=True)
    libelle = models.CharField(max_length=255, null=True, blank=False)
    personnalise = models.BooleanField(default=False, null=True)
    nbReading = models.IntegerField(null=True)
    nbListening = models.IntegerField(null=True)
    niveauBulats = models.ForeignKey(Niveau, on_delete=models.CASCADE, null=True)
    typeTest = models.ForeignKey(TypeTest, on_delete=models.CASCADE, null=False)


class EtudiantTest(ModelBase):
    dateDebut = models.DateTimeField("Date de début", null=True)
    dateFin = models.DateTimeField("Date de fin", null=True)
    score = models.IntegerField(null=True)
    testBulats = models.ForeignKey(Test, on_delete=models.CASCADE, null=False)
    etudiant = models.ForeignKey(User, on_delete=models.CASCADE, null=False)

    
class Historique(ModelBase):
    libelle = models.CharField(null=False, blank=False, max_length=255)
    testBulats = models.ForeignKey(Test, on_delete=models.CASCADE, null=False)


class Tag(ModelBase):
    libelle = models.CharField(null=False, blank=False, max_length=255) # pour dire des choses comme if/clauses etc
    description = models.TextField(null=True, blank=True)  
    typeTag = models.CharField(null=False, blank=False, max_length=255) # pour dire Vocabulary, Grammar etc


class TypeQuestion(ModelBase):
    categorie = models.CharField(null=False, blank=False, max_length=255)
    libelle = models.CharField(null=False, blank=False, max_length=255)
    description = models.TextField(null=True, blank=True)


class Texte(ModelBase):
    titre = models.CharField(null=False, blank=False, max_length=255)


class Question(ModelBase):
    libelle = models.CharField(null=False, blank=False, max_length=255)
    description = models.TextField(null=True, blank=True)  
    explication = models.TextField(null=True, blank=True)  
    numeroOrdre = models.IntegerField(null=True)
    fichierAudio = models.FileField(upload_to="User/Question", blank=True, null=True)
    nb_point = models.IntegerField(null=True)
    fichier = models.FileField(upload_to="User\Question", blank=True, null=True)

    texteBulats = models.ForeignKey(Texte, on_delete=models.CASCADE, null=True, blank=False)
    typeQuestion = models.ForeignKey(TypeQuestion, on_delete=models.CASCADE, null=False)
    niveauBulats = models.ForeignKey(Niveau, on_delete=models.CASCADE, null=True)


class QuestionTag(ModelBase):
    questionBulats = models.ForeignKey(Question, on_delete=models.CASCADE, null=False)
    tagBulats = models.ForeignKey(Tag, on_delete=models.CASCADE, null=False)


class TestTag(ModelBase):
    testBulats = models.ForeignKey(Test, on_delete=models.CASCADE, null=False)
    tagBulats = models.ForeignKey(Tag, on_delete=models.CASCADE, null=False)


class Proposition(ModelBase):
    libelle = models.CharField(null=False, blank=False, max_length=255)
    isCorrect = models.BooleanField(default=False, null=True)

    questionBulats = models.ForeignKey(Question, on_delete=models.CASCADE, null=False)


class Reponse(ModelBase):
    libelle = models.CharField(null=False, blank=False, max_length=255)
    juste = models.BooleanField(default=False, null=True)
    date_debut = models.DateTimeField("Date de fin", null=True)
    date_fin = models.DateTimeField("Date de fin", null=True)
    etudiantTest = models.ForeignKey(EtudiantTest, on_delete=models.CASCADE, null=False)
    questionBulats = models.ForeignKey(Question, on_delete=models.CASCADE, null=False)
