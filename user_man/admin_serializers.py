from rest_framework import serializers
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth.models import update_last_login
from rest_framework.permissions import IsAuthenticated
from .models import *

class AdminUserSerializer(serializers.ModelSerializer):
    username = serializers.models.CharField(max_length=50)
    password = serializers.models.CharField(max_length=50)
    class Meta:
        model = Profil
        fields = ['id', 'user', 'username', 'password', 'email', 'nom', 'prenoms', 'dateNaissance', 'contact', 'role', 'sexe', 'avatar', 'actif', 'valide', 'specialite', 'fonction', 'codeEnseignant', 'codeEtudiant', 'niveauBulats',  'created_at', 'updated_at', 'deleted', 'deleted_at']
        extra_kwargs = {
            'user': {'write_only': True},
            'password': {'write_only': True},
        }

    def create(self, validated_data):
        utilisateur = Profil(
            email=validated_data['email'],
            nom=validated_data['nom'],
            prenoms=validated_data['prenoms'],
            dateNaissance=validated_data['dateNaissance'],
            contact=validated_data['contact'],
            role=validated_data['role'],
            sexe=validated_data['sexe'],
            avatar=validated_data['avatar'],
            actif=validated_data['actif'],
            valide=validated_data['valide'],
            specialite=validated_data['specialite'],
            fonction=validated_data['fonction'],
            codeEnseignant=validated_data['codeEnseignant'],
            codeEtudiant=validated_data['codeEtudiant'],
            niveauBulats=validated_data['niveauBulats'],
            created_at=validated_data['created_at'],
            updated_at=validated_data['updated_at'],
            deleted=validated_data['deleted'],
            deleted_at=validated_data['deleted_at'],
        )
        user = User(
            username=validated_data['username'],
            email=validated_data['email'],
        )
        user.set_password(validated_data['password'])
        user.is_superuser = True
        user.is_staff = True
        user.save()
        Profil.objects.create(user=user, **utilisateur)
        return utilisateur

    def update(self, instance, validated_data):
        user = instance.user

        instance.email=validated_data['email']
        instance.nom=validated_data['nom']
        instance.prenoms=validated_data['prenoms']
        instance.dateNaissance=validated_data['dateNaissance']
        instance.contact=validated_data['contact']
        instance.role=validated_data['role']
        instance.sexe=validated_data['sexe']
        instance.avatar=validated_data['avatar']
        instance.actif=validated_data['actif']
        instance.valide=validated_data['valide']
        instance.specialite=validated_data['specialite']
        instance.fonction=validated_data['fonction'],
        instance.codeEnseignant=validated_data['codeEnseignant']
        instance.codeEtudiant=validated_data['codeEtudiant']
        instance.niveauBulats=validated_data['niveauBulats']
        instance.created_at=validated_data['created_at']
        instance.updated_at=validated_data['updated_at']
        instance.deleted=validated_data['deleted']
        instance.deleted_at=validated_data['deleted_at']
        instance.save()

        # instance.username = validated_data.get('username', instance.username)

        user.username = validated_data['username']
        user.set_password(validated_data['password'])
        user.save()
        return instance
