from django.shortcuts import render
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework import permissions, status, viewsets
from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from .serializers import *
from .ressources import *
from .models import *
from tablib import Dataset
from rest_framework.parsers import FileUploadParser
import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta, MO

class UserLogin(APIView):
    """return user token if user credetials are correct"""
    serializer_class = UserTokenSerializer

    def get(self, request, format=None):
        """user sign in form"""
        serializer = UserTokenSerializer()
        return Response(status=status.HTTP_200_OK)

    def post(self, request, format=None):
        """post user request"""
        serializer = UserTokenSerializer(data=request.data)

        if not request.data.get('username') or not request.data.get('password'):
            return Response({"reponse": "Nom d'utilisateur et mot de passe requis"}, status=status.HTTP_406_NOT_ACCEPTABLE)

        if serializer.is_valid():
            user = authenticate(
                username=serializer.data.get('username'),
                password=serializer.data.get('password'))
            if user is not None:
                token, create_or_fetch = Token.objects.get_or_create(
                    user=user)
                return Response({'token': token.key, 'user': {'id': token.user.id, 'username': token.user.username, 'email': token.user.email}}, status=status.HTTP_200_OK)
            msg = 'Identifiants incorrects'
            return Response({'message': msg}, status=status.HTTP_401_UNAUTHORIZED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profil.objects.filter(deleted=False)
    serializer_class = UserProfilSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    # Methodes suplementaire de compte start
    def destroy(self, request, pk=None):
        objet = Profil.objects.get(pk=pk)
        try:
            # objet.deleted = True
            # objet.deleted_at = timezone.now()
            objet.suppress()
            objet.save()
            return Response({'status': 'Suppression faite'})
        except:
            return Response({'status': 'Erreur lors de la suppression'})

    # @action(detail=True, permission_classes=[permissions.IsAuthenticatedOrReadOnly])
    def restore(self, request, pk=None):
        objet = Profil.objects.get(pk=pk)
        try:
            # objet.deleted = False
            # objet.deleted_at = None
            objet.restore()
            objet.save()
            return Response({'status': 'Les données ont été restaurées avec succes'})
        except:
            return Response({'status': 'Erreur lors de la suppression'})

    # @action(detail=False, permission_classes=[permissions.IsAuthenticatedOrReadOnly])
    def get_deleted_list(self, request):
        objet = Profil.objects.filter(deleted=True).order_by('-deleted_at')
        serializer = self.get_serializer(objet, many=True)
        return Response(serializer.data)


class UserViewSet(viewsets.ViewSet):
    # permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    def list(self, request):
        if request.headers.get('role'):
            users = User.objects.filter(profil__deleted=False, profil__role=request.headers.get('role'))
        else:
            users = User.objects.filter(profil__deleted=False)
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)

    def create(self, request):
        profil = Profil()
        profil.contact = request.data.get("contact")
        profils = Profil.objects.filter(contact=profil.contact, deleted=False)

        if not request.data.get('username') or not request.data.get('password'):
            return Response({"reponse": "Nom d'utilisateur et mot de passe requis"}, status=status.HTTP_406_NOT_ACCEPTABLE)

        if not request.data.get('nom') or not request.data.get('prenoms'):
            return Response({"reponse": "Le nom et les prénoms sont requis"}, status=status.HTTP_408_REQUEST_TIMEOUT)

        if len(profils) > 0:
            return Response({"reponse": "Il existe déjà un utilisateur avec ce contact"}, status=status.HTTP_409_CONFLICT)
        if len(request.data.get('password')) < 8:
            return Response({'reponse': 'Veuillez un mot de passe contenant au moins 8 caractères'}, status=status.HTTP_410_GONE)
        if request.data.get('role') is None:
            return Response({"reponse": "Le profil de l'utilisateur n'a pas été renseigné"}, status=status.HTTP_414_REQUEST_URI_TOO_LONG)
        if request.data.get('email') is None:
            return Response({"reponse": "L'email est requis"}, status=status.HTTP_402_PAYMENT_REQUIRED)
        users = User.objects.filter(username=request.data.get('username'))
        if len(users) > 0:
            return Response({"reponse": "Un utilisateur possède déjà ce nom d'utilisateur"}, status=status.HTTP_413_REQUEST_ENTITY_TOO_LARGE)
        
        serializer = ProfilSerializer(profil, data=request.data)
        if serializer.is_valid():
            user = User.objects.create_user(request.data.get('username'), request.data.get('email'), request.data.get('password'))
            profil.user = user
            serializer = ProfilSerializer(profil, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):

        user = User.objects.get(pk=pk)

        if request.headers.get('action') and request.headers.get('action') == "changePassword":
            if not request.data.get('oldPassword'):
                return Response({"reponse": "Veuillez renseigner l'ancien mot de passe"}, status=status.HTTP_402_PAYMENT_REQUIRED)
            if not request.data.get('newPassword') or len(request.data.get('newPassword')) < 8:
                return Response({"reponse": "Veuillez renseigner le nouveau mot de passe de tel sorte que ça contienne au moins 8 caractères"}, status=status.HTTP_406_NOT_ACCEPTABLE)

            if not user.check_password(request.data.get('oldPassword')):
                return Response({"reponse": "Le mot de passe courant saisi est erronée! Veuillez réessayer"}, status=status.HTTP_405_METHOD_NOT_ALLOWED)
            user.set_password(request.data.get('newPassword'))
            user.save()
            return Response(status=status.HTTP_204_NO_CONTENT)

        if request.data.get('username') and request.data.get('username') != '':
            user.username = request.data.get('username')
        if request.data.get('email') and request.data.get('email') != '':
            user.email = request.data.get('email')

        if not user.profil:
            return Response({"reponse": "L'utilisateur ci ne peut être modifié car n'a pas d'informations personnelles et n'a pas de rôles sur la plateforme"}, status=status.HTTP_401_UNAUTHORIZED)

        serializer = ProfilSerializer(user.profil, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            user.save()
            userSerializer = UserSerializer(user)
            return Response(userSerializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    # Methodes suplementaire de compte start
    def destroy(self, request, pk=None):
        objet = Profil.objects.get(pk=pk)
        try:
            # objet.deleted = True
            # objet.deleted_at = timezone.now()
            objet.suppress()
            objet.save()
            return Response({'status': 'Suppression faite'})
        except:
            return Response({'status': 'Erreur lors de la suppression'})

    def restore(self, request, pk=None):
        objet = Profil.objects.get(pk=pk)
        try:
            # objet.deleted = False
            # objet.deleted_at = None
            objet.restore()
            objet.save()
            return Response({'status': 'Les données ont été restaurées avec succes'})
        except:
            return Response({'status': 'Erreur lors de la suppression'})

    def get_deleted_list(self, request):
        profil = User.objects.filter(profil__deleted=False)
        objet = Profil.objects.filter(deleted=True).order_by('-deleted_at')
        serializer = ProfilSerializer(profil, data=request.data)
        return Response(serializer.data)


class SalonViewSet(viewsets.ViewSet):

    def list(self, request):
        if request.headers.get("enseignantId"):
            salons = Salon.objects.filter(deleted=False, enseignant_id=request.headers.get("enseignantId"))
        elif request.headers.get("statut"):
            salons = Salon.objects.filter(deleted=False, statut=request.headers.get("statut"))
        else:
            salons = Salon.objects.filter(deleted=False)
        serializer = SalonSerializer(salons, many=True)
        return Response(serializer.data)
    
    def create(self, request):
        salon = Salon()
        if request.data.get("enseignantId"):
            enseignant = User.objects.get(pk=request.data.get("enseignantId"))
            salon.enseignant = enseignant
        else:
            return Response({"response": "L'id de l'enseignant est obligatoire"}, status=status.HTTP_406_NOT_ACCEPTABLE)

        if not request.data.get("nom"):
            return Response({"response": "Veuillez renseigner le nom du salon"}, status=status.HTTP_408_REQUEST_TIMEOUT)

        serializer = SalonSerializer(salon, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):

        salon = Salon.objects.get(pk=pk)
        if request.data.get("nom") and request.data.get("nom") != "":
            salon.nom = request.data.get("nom")
        if request.data.get("statut") and request.data.get("statut") != "":
            salon.statut = request.data.get("statut")
        if request.data.get("objectifs"):
            salon.objectifs = request.data.get("objectifs")

        serializer = SalonSerializer(salon, request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        salons = Salon.objects.filter(deleted=False)
        salon = get_object_or_404(salons, pk=pk)
        serializer = SalonSerializer(salon)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        salon = Salon.objects.get(pk=pk)
        salon.deleted = True
        salon.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class EtudiantSalonViewSet(viewsets.ViewSet):

    def list(self, request):
        if request.headers.get("salonId"):
            etudiantsalons = EtudiantSalon.objects.filter(deleted=False, salonBulats_id=request.headers.get("salonId"))
        elif request.headers.get("etudiantId"):
            etudiantsalons = EtudiantSalon.objects.filter(deleted=False, salonBulats_id=request.headers.get("etudiantId"))
        else:
            return Response({"response": "Veuillez envoyer un paramètre de tri, soit l'id du salon ou soit celui de l'étudiant"}, status=status.HTTP_406_NOT_ACCEPTABLE)

        
        serializer = EtudiantSalonSerializer(etudiantsalons, many=True)
        return Response(serializer.data)

    def create(self, request):
        etudiantSalon = EtudiantSalon()
        if request.data.get("etudiantId"):
            etudiant = User.objects.get(pk=request.data.get("etudiantId"))
            etudiantSalon.etudiant = etudiant
        else:
            return Response({"reponse": "L'id de l'etudiant est requis'"}, status=status.HTTP_406_NOT_ACCEPTABLE)
            
        if request.data.get("salonId"):
            salons = Salon.objects.filter(id=request.data.get("salonId"))
            if len(salons) <= 0:
                return Response({"reponse": "Ce salon n'existe pas"}, status=status.HTTP_412_PRECONDITION_FAILED)
            etudiantSalon.salonBulats = salons[0]
        else:
            return Response({"reponse": "L'id du salon est requis'"}, status=status.HTTP_408_REQUEST_TIMEOUT)

        serializer = EtudiantSalonSerializer(etudiantSalon, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        if request.headers.get("salonId") and request.headers.get("etudiantId"):
            etudiantSalons = EtudiantSalon.objects.filter(etudiant_id=request.headers.get("etudiantId"), salonBulats_id=request.headers.get("salonId"))
            if len(etudiantSalons > 0):
                etudiantSalons[0].deleted = True
                etudiantSalons[0].save()
        else: 
            etudiantSalon = EtudiantSalon.objects.get(pk=pk)
            etudiantSalon.deleted = True
            etudiantSalon.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class InvitationViewSet(viewsets.ViewSet):

    def list(self, request):
        if request.headers.get("salonId"):
            invitations = Invitation.objects.filter(deleted=False, salonBulats_id=request.headers.get("salonId"))
        elif request.headers.get("etudiantId"):
            invitations = Invitation.objects.filter(deleted=False, salonBulats_id=request.headers.get("etudiantId"))
        else:
            return Response({"response": "Veuillez envoyer un paramètre de tri, soit l'id du salon ou soit celui de l'étudiant"}, status=status.HTTP_406_NOT_ACCEPTABLE)

        
        serializer = InvitationSerializer(invitations, many=True)
        return Response(serializer.data)

    def create(self, request):
        invitation = Invitation()
        if request.data.get("etudiantId"):
            etudiant = User.objects.get(pk=request.data.get("etudiantId"))
            invitation.etudiant = etudiant
        else:
            return Response({"reponse": "L'id de l'etudiant est requis'"}, status=status.HTTP_406_NOT_ACCEPTABLE)
            
        if request.data.get("salonId"):
            salon = Salon.objects.get(pk=request.data.get("salonId"))
            invitation.salonBulats = salon
        else:
            return Response({"reponse": "L'id du salon est requis'"}, status=status.HTTP_408_REQUEST_TIMEOUT)

        serializer = InvitationSerializer(invitation, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def retrieve(self, request, pk=None):
    #     if request.headers.get("salonId") and request.headers.get("etudiantId"):
    #         invitations = Invitation.objects.filter(etudiant_id=request.headers.get("etudiantId"), salonBulats_id=request.headers.get("salonId"))
    #         if len(invitations <= 0):
    #             return Response({"reponse": "Aucune invitation trouvée"}, status=status.HTTP_410_GONE)
    #     else: 
    #         invitation = Invitation.objects.get(pk=pk)
    #     return Response(status=status.HTTP_204_NO_CONTENT)

    def destroy(self, request, pk=None):
        if request.headers.get("salonId") and request.headers.get("etudiantId"):
            invitations = Invitation.objects.filter(etudiant_id=request.headers.get("etudiantId"), salonBulats_id=request.headers.get("salonId"))
            if len(invitations > 0):
                invitations[0].deleted = True
                invitations[0].save()
        else: 
            invitation = Invitation.objects.get(pk=pk)
            invitation.deleted = True
            invitation.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class EnseignantTestViewSet(viewsets.ViewSet):

    def list(self, request):
        if request.headers.get("enseignantId"):
            tests = Test.objects.filter(deleted=False, enseignant_id=request.headers.get("enseignantId"))
        elif request.headers.get("statut"):
            tests = Test.objects.filter(deleted=False, statut=request.headers.get("statut"))
        else:
            tests = Test.objects.filter(deleted=False)
        serializer = SalonSerializer(tests, many=True)
        return Response(serializer.data)
    
    def create(self, request):
        test = Test()
        if request.data.get("enseignantId"):
            enseignant = User.objects.get(pk=request.data.get("enseignantId"))
            test.enseignant = enseignant
        else:
            return Response({"response": "L'id de l'enseignant est obligatoire"}, status=status.HTTP_406_NOT_ACCEPTABLE)

        if not request.data.get("nom"):
            return Response({"response": "Veuillez renseigner le nom du test"}, status=status.HTTP_408_REQUEST_TIMEOUT)

        serializer = SalonSerializer(test, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):

        test = Test.objects.get(pk=pk)
        if request.data.get("nom") and request.data.get("nom") != "":
            test.nom = request.data.get("nom")
        if request.data.get("statut") and request.data.get("statut") != "":
            test.statut = request.data.get("statut")
        if request.data.get("objectifs"):
            test.objectifs = request.data.get("objectifs")

        serializer = SalonSerializer(test, request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        tests = Test.objects.filter(deleted=False)
        test = get_object_or_404(tests, pk=pk)
        serializer = SalonSerializer(test)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        test = Test.objects.get(pk=pk)
        test.deleted = True
        test.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

